package io.magician.tcp.codec.impl.websocket.constant;

/**
 * Socket事件枚举
 */
public enum WebSocketEnum {

    OPEN, CLOSE, MESSAGE
}
