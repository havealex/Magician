package io.magician.common.constant;

/**
 * 事件执行器枚举
 */
public class EventEnum {

    /**
     * 是否允许窃取其他执行器的任务
     */
    public enum STEAL {
        YES, NO
    }

}
